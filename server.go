package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	addr = flag.String("addr", ":12345", "websocket address")
)

func main() {
	flag.Parse()

	go n.run()

	http.HandleFunc("/ws", websocketHandler)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
