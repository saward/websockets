var socket;
var last_error;

// Possibly better ways you can do this, I'm not sure.  But you do need to know each element if you want to change it:
var   connectButton         = document.getElementById('connectButton'),
      serverAddress         = document.getElementById('serverAddress'),
      usernameInput         = document.getElementById('usernameInput'),
      connectDiv 			= document.getElementById('connectDiv'),
      mainGameDiv			= document.getElementById('mainGameDiv'),
      fillMeText			= document.getElementById('fillMeText'),
      myList				= document.getElementById('myList'),
      fillMeButton			= document.getElementById('fillMeButton'),
      errorsList 			= document.getElementById('errorsList');



// Stuff for connecting:
connectButton.onclick = function(e) {
	console.log("Connect button clicked");
	connect();
};

fillMeButton.onclick = function(e) {
	socket.send("2|"+fillMeText.value);
};

function addLI(list, text) {
	var li = document.createElement("li");
	var text = document.createTextNode(text);
	li.appendChild(text);
	list.appendChild(li);
};

function connect(){ 
	var socket_addr = 'ws://' + serverAddress.value + "/ws";
	socket = new WebSocket(socket_addr);

	socket.onmessage = function(mess) { onMessage(mess); };
	socket.onopen = function(evt) { onOpen(evt) }; 
	socket.onclose = function(evt) { onClose(evt) };
	socket.onerror = function(evt) { onError(evt) };
	connectDiv.style.display = 'none';
	mainGameDiv.style.display = 'inline';
};

function onMessage(mess) {
	console.log("Message: "+mess.data);
	var mess_details = mess.data.split("|");
	if (mess_details[0] == 2) {
		addLI(myList, mess_details[1])
	}
};

function onOpen(evt) {
	// This sends our character or user name to the server upon connection
	socket.send("1|" + usernameInput.value);
};

function onClose(evt) {
};

function onError(evt) {
	console.log("WARNING:  Socket error received: " + evt);
	addLI(errorsList, "Socket error received.  Socket readyState: " + socket.readyState);
	last_error = evt;
};