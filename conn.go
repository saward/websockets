package main

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

type Connection struct {
	ws *websocket.Conn

	// Channel for sending messages to this socket
	send chan []byte
}

func (c *Connection) reader() {

	for {
		_, message, err := c.ws.ReadMessage()

		if err != nil {
			break
		}

		n.incoming <- ClientMessage{c, message}
	}
	c.ws.Close()
}

func (c *Connection) writer() {
	for message := range c.send {

		err := c.ws.WriteMessage(websocket.TextMessage, message)

		if err != nil {
			break
		}
	}
	c.ws.Close()
}

func websocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(w, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		log.Println(err)
		return
	}

	c := &Connection{send: make(chan []byte, 200), ws: conn}
	n.register <- c

	go c.writer()
	c.reader()

	n.unregister <- c
}
