package main

import (
	"fmt"
	"log"
	"strings"
)

type ClientMessage struct {
	Conn *Connection
	Data []byte
}

type Nexus struct {
	// Registered connections
	connections map[*Connection]bool

	// Inbound messages from the connections
	incoming chan ClientMessage

	// Outbound messages for a particular connection
	outgoing chan ClientMessage

	// Send a message to all clients
	broadcast chan []byte

	// Register connections
	register chan *Connection

	// Unregister connections
	unregister chan *Connection
}

var n = Nexus{
	incoming:    make(chan ClientMessage),
	outgoing:    make(chan ClientMessage),
	broadcast:   make(chan []byte),
	register:    make(chan *Connection),
	unregister:  make(chan *Connection),
	connections: make(map[*Connection]bool),
}

func (n *Nexus) run() {
	log.Printf("Starting nexus running\n")
	for {

		select {
		case c := <-n.register:
			n.connections[c] = true
			log.Printf("Connection opened")
			c.send <- []byte("Tester")
		case c := <-n.unregister:
			delete(n.connections, c)
			close(c.send)
			log.Printf("Connection closed")
		case cm := <-n.incoming:
			if cm.Data[len(cm.Data)-1] == '\n' {
				cm.Data = cm.Data[:len(cm.Data)-1]
			}

			parts := strings.Split(string(cm.Data), "|")
			if len(parts) > 0 {
				switch parts[0] {
				case "1":
					log.Printf("Player connection: %s\n", parts[1])
				case "2":
					msg := fmt.Sprintf("2|From me, the server: %s", parts[1])
					cm.Conn.send <- []byte(msg)
				}
			}
		case cm := <-n.outgoing:
			cm.Conn.send <- cm.Data
		case m := <-n.broadcast:
			for c, b := range n.connections {
				if b {
					c.send <- m
				}
			}
		}
	}
}
